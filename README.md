# XInclude and XML Compare
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

An example pipeline that instructs the XML parser to process XInclude statements before passing the input data to XML Compare.

This document describes how to run the sample. For concept details see: [XInclude and XML Compare](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/xinclude-and-xml-compare)

## DXP sample
If you have Ant installed, use the build script provided to run the sample. Simply type the following command to run the pipeline and produce the output files non-include-result.xml and include-result.xml.
  
	ant run-command-line

If you don't have Ant installed, you can run the sample from a command line by issuing the following command from the sample directory (ensuring that you use the correct directory separators for your operating system).  Replace x.y.z with the major.minor.patch version number of your release e.g. deltaxml-10.0.0.jar

	java -jar ../../deltaxml-x.y.z.jar compare xinclude-demo inputs/input1-full.xml inputs/input2-includes.xml non-include-result.xml enable-xinclude=false
	java -jar ../../deltaxml-x.y.z.jar compare xinclude-demo inputs/input1-full.xml inputs/input2-includes.xml include-result.xml enable-xinclude=true

## Java API sample
If you have Ant installed, use the build script provided to run the sample. Simply type the following command to run the pipeline and produce the output files non-systemid-result.xml and systemid-result.xml.

	ant run-api

If you don't have Ant installed, you can run the sample from a command line by issuing the following commands from the sample directory (ensuring that you use the correct directory and class path separators for your operating system), replacing x.y.z with the major.minor.patch version number of your release e.g. deltaxml-10.0.0.jar:

	javac -cp ../../deltaxml-x.y.z.jar -d class XincludeWithPipelinedComparator.java
	java -cp class:../../deltaxml-x.y.z.jar:../../saxon9pe.jar XincludeWithPipelinedComparator

To clean up the sample directory, run the following Ant command.

	ant clean