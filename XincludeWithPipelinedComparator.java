// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.Reader;
import java.util.Map;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.InputSource;

import com.deltaxml.core.DXPConfiguration;
import com.deltaxml.core.PipelinedComparator;

public class XincludeWithPipelinedComparator {
  
  public static void main(String [] args) throws Exception {
    File dxpFile= new File("xinclude.dxp");
    DXPConfiguration dxp= new DXPConfiguration(dxpFile);
    Map<String, Boolean> booleanOverrides= dxp.getBooleanParameters();
    booleanOverrides.put("enable-xinclude", true);
    PipelinedComparator pc= dxp.generate(booleanOverrides, null);
    
    File input1= new File("inputs/input1-full.xml");
    File input2= new File("inputs/input2-includes.xml");
    
    //construct StreamSources from the input files
    StreamSource ss1= new StreamSource(new FileInputStream(input1));
    StreamSource ss2= new StreamSource(new FileInputStream(input2));
    
    //now compare the stream sources without first setting their system id
    System.out.println("Comparing StreamSources with no systemIds...");
    pc.compare(ss1, ss2, new StreamResult(new File("non-systemid-result.xml")));
    System.out.println("Success");
    
    //reinitialise the StreamSources
    ss1= new StreamSource(new FileInputStream(input1));
    ss2= new StreamSource(new FileInputStream(input2));
    
    //now set the system ids on the StreamSources
    ss1.setSystemId(input1.getAbsolutePath());
    ss2.setSystemId(input2.getAbsolutePath());
    
    //and compare again
    System.out.println("Comparing StreamSources with systemIds declared...");
    pc.compare(ss1, ss2, new StreamResult(new File("systemid-result.xml")));
    System.out.println("Success");
  }
  
}